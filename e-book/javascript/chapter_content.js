// ----------引入 json

const jsonApi =
    "https://raw.githubusercontent.com/YUN-RU-TSENG/src/master/book.json";

fetch(jsonApi)
    .then(function(response) {
        return response.json();
    })
    .then(function(json) {
        jsonText(json);
    });

// ---------- 將文字打入

function jsonText(json) {
    const page = document.getElementById("container");
    json.forEach(obj => {
        const chapter = obj.chapter;
        const chapterElement = document.createElement("h2");
        const chapterTextNode = document.createTextNode(chapter);
        chapterElement.appendChild(chapterTextNode);
        page.appendChild(chapterElement);
        const section = obj.sections;
        section.forEach(objContent => {
            const contents = objContent.content;
            const contentElement = document.createElement("p");
            contentElement.classList.add("text");
            const contentText = document.createTextNode(contents);
            contentElement.appendChild(contentText);
            page.appendChild(contentElement);
        });
    });
}

const prefPage = document.getElementById("prefPage");
const nextPage = document.getElementById("nextPage");
const container = document.getElementById("container");
const card = document.getElementById("card");
const vertical = document.getElementById("vertical");

var add = 0;
var pageWidth = 696;
var countNumber = 57;
var countNumberCalaute = countNumber - 1;
var count = pageWidth * countNumberCalaute;


let screenLog = document.querySelector("#card");

//----------- 點擊畫面 0/3~1/3 區間出現 navbar
$(".page-container").click(function(e) {
    if ((e.pageX >= 0) && (e.pageX <= $(window).width() / 3)) {
        if (add === 0) return;
        add += pageWidth;
        container.style.transform = `translateX(${add}px)`;
    }
});
//----------- 點擊畫面 1/3~2/3 區間出現 navbar
$(".page-container").click(function(e) {
    if ((e.pageX >= $(window).width() / 3 * 2) && (e.pageX <= $(window).width())) {
        if (add === -count) return;
        add += -pageWidth;
        container.style.transform = `translateX(${add}px)`;
    }
});

const missionCheckArea = document.getElementsByTagName("body")[0];
const missionCheckItem = document.getElementsByClassName("bgc-b")[0];

$(".previous, .btn-navbar").click(function() {
    $(".lightbox-shadow, .lightbox-leave").toggle()
})

$(".leave1-1").click(function() {
    $(".lightbox-leave, .lightbox-shadow").toggle()
})

$(".leave1-2").click(function() {
    $(".lightbox-leave, .lightbox-leave2").toggle()
})

$(".leave2-1").click(function() {
    $(".lightbox-leave2, .lightbox-leave3").toggle()
})

$(".leave2-2").click(function() {
    $(".lightbox-leave2, .lightbox-shadow").toggle()
})

$(".leave3-2").click(function() {
    $(".lightbox-leave3, .lightbox-shadow").toggle()
})

$(".settings").click(function() {
    $(".lightbox-shadow, .lightbox-font").toggle()
})

$("btn-navbar").click(function() {
    $(".lightbox-shadow, lightbox-leave").toggle()
})