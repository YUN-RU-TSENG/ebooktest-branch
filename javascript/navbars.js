//----------- 點擊畫面 1/3~2/3 區間出現 navbar
$(".page-container").click(function(e) {
    if ((e.pageX >= $(window).width() / 3) && (e.pageX <= $(window).width() / 3 * 2)) {
        $("#toolbar,#back").toggle()
    }
});

//-------點擊 burger 出現 menu----------
$(".burger").click(function() {
    $(".burger-menu, .burger-menu-shadow").toggle();
});
//------- 點擊 Aa 出現 font settings ----------
// $(".settings").click(function() {
//     $(".lightbox-shadow, .lightbox-container").toggle()
// })

//-------點擊 note 出現 note----------
$(".note").click(function() {
    $(".note-window, .note-window-shadow").toggle();
});

//-------點擊 share 出現 share---------

$(".share, .add-share").click(function() {
    $(".share-window-shadow, .share-window").toggle();
});

//----------點擊 note window 裡的 dropdown，出現 tooltips --------
$(".dropdown").click(function() {
    $(".tooltip-pulldown-wrapper").toggle();
});

//-----------------換螢光筆顏色-----------------

$(".yellow").click(function() {
    $(".color").css("backgroundColor", "#faf096");
});
$(".red").click(function() {
    $(".color").css("backgroundColor", "#f9989f");
});
$(".purple").click(function() {
    $(".color").css("backgroundColor", "#c4beff");
});
$(".green").click(function() {
    $(".color").css("backgroundColor", "#c5f8c8");
});
//------------在 note window 裡面新增註解(新的彈跳視窗)---------------
$(".add-note").click(function() {
    $(".row-comment-wrapper").toggle();
});

// --------按取消關閉註解彈跳視窗------------
$("#cancel, #done").click(function() {
    $(".row-comment-wrapper, .tooltip-pulldown-wrapper").css("display", "none");
});

//----------刪除註解------------
$(".add-delete").click(function() {
    $(".color, .selected, .tooltip-pulldown-wrapper, .dropdown-wrapper, .rect-wrapper").css("display", "none");
});


//--------------文字大小縮放------------
var num = 20;
$(".fz-small").click(function() {
    if ((num > 12) && (num < 21)) {
        num -= 2;
    } else if ((num > 20) && (num < 25) || (num > 48)) {
        num -= 4;
    } else if ((num > 24) && (num < 48) || (num > 23) && (num <= 48)) {
        num -= 6;
    }
    $(".page-container p").css("font-size", num);
});

$(".fz-big").click(function() {
    if (num < 20) {
        num += 2;
    } else if (((num > 19) && (num < 24)) || ((num > 47) && (num < 52))) {
        num += 4;
    } else if ((num >= 24) && (num < 48)) {
        num += 6;
    }
    $(".page-container p").css("font-size", num);
});


//----------font settings-換背景顏色---------------
$(".bgc-w").click(function() {
    $(".page-container").css("backgroundColor", "#fff");
    $(".page-container p,.page-container h2,.previous").css("color", "#595959");
});
$(".bgc-b").click(function() {
    $(".page-container").css("backgroundColor", "rgb(0, 0, 0)");
    $(".page-container p,.page-container h2,.previous").css("color", "#fff");
});
$(".bgc-y").click(function() {
    $(".page-container").css("backgroundColor", "#fbf0da");
    $(".page-container p,.page-container h2,.previous").css("color", "#595959");
});
$(".bgc-g").click(function() {
    $(".page-container").css("backgroundColor", "#c5e7ce");
    $(".page-container p,.page-container h2,.previous").css("color", "#595959");
});

//----------- 點擊空白處關閉漢堡選單 ---------
// ------ 點擊 .chapter1 為暫時性，展示用----
$(".burger-menu-shadow,.back, .catalogue,li").click(function() {
    $(".burger-menu, .burger-menu-shadow").css("display", "none");
});

//----------- 點擊空白處關閉 font settings ----------
$(".lightbox-shadow").click(function() {
    $(".lightbox-container, .lightbox-shadow").css("display", "none");
});

//-----------點擊關閉 note--------------

$(".close-note, .note-window-shadow").click(function() {
    $(".note-window, .note-window-shadow, .tooltip-pulldown-wrapper").css("display", "none");
});
//----------- 點擊關閉 share------------
$(".share-window-shadow, .burger, .settings, .note").click(function() {
    $(".share-window-shadow, .share-window").css("display", "none");
});

// -------針對選取文字加入註解---------------
$("#done").click(function() {
    var text = $("#comment").val();
    $(".comment-wrapper .comment").text(text);
    if (text != "") {
        $(".rect").css("display", "inline-block")
    } else {
        $(".rect").css("display", "none")
    }
});

//------------變換 font-family-----------------

$(".ffamily").change(function() {
    $(".page-container p, #text-container h2").css("font-family", $(this).val());
});


//----------------------------task1 成功判斷-----------------------------
// 1. 完成任務1-2、1-3
var num = 20;
$(".chapter1").click(function() {
   $(".bgc-b").click(function() {
       if (num == 52) {
           $(".lightbox-mission").toggle();
           $(".lightbox-leave-shadow ").toggle();
       }
   });
})

//------- 進入文章頁面至完成任務 1-1 計時器 -------------
// var time1 = 0;

// $(document).ready(function() {
//     var counter1 = 0;

//     function count1() {
//         time1 += 0.1
//         $(".chapter1").click(function() {
//             clearInterval(timer1)
//         })
//     }
//     if (time2 == 0) {
//         $(document).click(function() {
//             counter1++;
//             $(".task1-1").text(time1.toFixed(2) + "秒/" + counter1 + "次");
//         });
//     } else {
//         return
//     }
//     var timer1 = setInterval(count1, 100)
// });


//------- 任務 1-1 ~ 1-2 計時器 & 點擊次數 -----------
var time2 = 0;
$(".chapter1").click(function() {
    var counter2 = 0;
    if (time3 == 0) {
        $(document).click(function() {
            counter2++;
        });
    }

    function count2() {
        time2 += 0.1
        if (num == 52) {
            clearInterval(timer2)
        }
        $(".task1-2").text(time2.toFixed(2) + "秒/" + counter2 + "次")
    }

    var timer2 = setInterval(count2, 100)
})


//------- 任務 1-2 ~ 1-3 計時器 & 點擊次數  -----------
var time3 = 0;
$(".fz-big").click(function() {
    var counter3 = 0;
    if (num == 52) {
        $(document).click(function() {
            counter3++;
        });
    }

    if ((num == 52) && (time3 == 0)) {
        function count3() {
            time3 += 0.1
            $(".bgc-b").click(function() {
                clearInterval(timer3);
            })
            $(".task1-3").text(time3.toFixed(2) + "秒/" + counter3 + "次");
        }
    }
    var timer3 = setInterval(count3, 100);
});